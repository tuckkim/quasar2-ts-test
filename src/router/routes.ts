import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Todo.vue'),
      },
      {
        path: '/forms',
        component: () => import('pages/Forms.vue'),
      },
      {
        path: '/qforms',
        component: () => import('pages/Qforms.vue'),
      },
      {
        path: '/help',
        component: () => import('pages/Help.vue'),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
