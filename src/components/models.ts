export interface Task {
  id: number;
  title: string;
  done: boolean;
}

export interface Todo {
  id: number;
  content: string;
}
export interface Meta {
  totalCount: number;
}

export interface VInput {
  validate: () => boolean;
  hasError: boolean;
}
